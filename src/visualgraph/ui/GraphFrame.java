/*
 * Project: 2D Graph Visualiser
 * Author: David Chaloupka
 * Crated: 2011-05-22
 * Last change: 2011-05-23
 */

package visualgraph.ui;

import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.event.*;

import visualgraph.Renderer;
import visualgraph.RendererObserver;

/**
 *
 * @author kvafy
 */
public class GraphFrame extends JPanel {
    private Renderer renderer;
    private int mouseLastX, mouseLastY;

    public GraphFrame(Renderer r) {
        this.renderer = r;
        final Renderer rend = r;

        // GUI actions handling
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                rend.setFrameSize(getWidth(), getHeight());
            }
        });
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent event) {
                mouseLastX = event.getX();
                mouseLastY = event.getY();
                if(javax.swing.SwingUtilities.isLeftMouseButton(event)) {
                    
                }
            }
        });
        this.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent event) {
                double D_ANGLE = Math.PI * 2 / 1000;
                double D_ZOOM = 1;
                int newX = event.getX(),
                    newY = event.getY();
                // delta angle by mouse movement
                double deltaByX = (newX - mouseLastX) * D_ANGLE,
                       deltaByY = (newY - mouseLastY) * D_ANGLE;
                mouseLastX = newX;
                mouseLastY = newY;
                if(javax.swing.SwingUtilities.isLeftMouseButton(event)) {
                    if(deltaByX != 0)
                        rend.setYRotation(rend.getYRotation() - deltaByX);
                    if(deltaByY != 0)
                        rend.setXRotation(rend.getXRotation() + deltaByY);
                }
                else if(javax.swing.SwingUtilities.isRightMouseButton(event)) {
                    if(deltaByX != 0)
                        rend.setZRotation(rend.getZRotation() + deltaByX);
                    if(deltaByY != 0)
                        rend.setZoom(rend.getZoom() - D_ZOOM * deltaByY);
                }
            }
        });
    }

    private void drawPixel(Graphics g, int x, int y, Color c) {
        g.setColor(c);
        g.drawLine(x, y, x, y);
    }

    @Override
    public void paint(Graphics graphics) {
        // simply render scene and display contents of framebuffer
        this.renderer.render(graphics);
        /*
        Color[][] framebuffer = this.renderer.getFramebuffer();
        for(int x = 0 ; x < framebuffer.length ; x++) {
            for(int y = 0 ; y < framebuffer[0].length ; y++) {
                this.drawPixel(graphics, x, y, framebuffer[x][y]);
            }
        }*/
    }
}
