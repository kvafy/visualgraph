/*
 * Project: 2D Graph Visualiser
 * Author: David Chaloupka
 * Crated: 2011-05-22
 * Last change: 2011-05-24
 */

package visualgraph;

/**
 *
 * @author kvafy
 */
public abstract class Function2D {
    private double[][] discontinuities;

    /**
     *
     * @param discontinuities Array of pairs [x,y] where function is undefined.
     */
    public Function2D(double[][] discontinuities) {
        this.discontinuities = discontinuities;
    }

    /** To be implemented in child. */
    protected abstract double value(double x, double y);

    public final boolean isDefined(double x, double y) {
        if(this.discontinuities != null) {
            final double TOLERANCE = 1e-3;
            for(int pair = 0 ; pair < this.discontinuities.length ; pair++) {
                double px = this.discontinuities[pair][0],
                       py = this.discontinuities[pair][1];
                if(Math.abs(x - px) + Math.abs(y - py) < TOLERANCE)
                    return false;
            }

        }
        return true;
    }

    /**
     *
     * @param x
     * @param y
     * @return  If function is not defined for [x,y], returns null. Otherwise
     *          returns f(x,y).
     */
    public final Double evaluate(double x, double y) {
        try {
            if(this.isDefined(x, y)) {
                Double value = this.value(x, y);
                if(value.isInfinite() || value.isNaN())
                    return null;
                else
                    return value;
            }
        }
        catch(Exception e) {
            // even though f(x,y) should be defined, it is not
            System.err.println("error evaluating function: ");
        }
        return null;
    }
}
