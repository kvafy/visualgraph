/*
 * Project: 2D Graph Visualiser
 * Author: David Chaloupka
 * Crated: 2011-05-22
 * Last change: 2011-05-25
 *
 * Changelog
 *   * 2011-05-24: ADDED  Backface culling works properly now.
 *   * 2011-05-23: CHANGE Added the Graphics argument to render method, because
 *                        copying the contents of framebuffer to window pixel
 *                        by pixel proved to be a serious bottleneck.
 */

package visualgraph;

import java.util.Arrays;
import java.util.LinkedList;
import java.awt.Color;
import java.awt.Graphics;
import visualgraph.primitives.*;

/**
 *
 * @author kvafy
 */
public class Renderer {
    // polygons to be rendered
    private PolygonMesh polygonMesh = new PolygonMesh();
    // buffers
    private int width, height;
    private Color[][] framebuffer;
    private double[][] zbuffer;
    // user given transformations (custom rotations)
    private double xrot, yrot, zrot;
    private double zoffset; // for "zoom"
    private double zcamera;
    private Color bgColor;
    // rendering options
    private boolean backfaceCulling;
    // other rendering related stuff
    private RendererObserver observer; // notify when scene has changed => need
                                       // to render and display the new scene
    private Graphics graphics = null;  // argument of render method (can be not
                                       // null only during invocation of this
                                       // method); when not null, the scene is
                                       // rendered directly withnout framebuffer



    public Renderer(int width, int height, Color bgColor) {
        this.observer = null;
        this.setFrameSize(height, width);
        this.bgColor = bgColor;
        // default settings
        this.backfaceCulling = false;
        this.xrot = this.yrot = this.zrot = 0;
        this.zoffset = 0;
        this.zcamera = -2;
    }
    
    public void setFrameSize(int width, int height) {
        this.width = width;
        this.height = height;
        this.framebuffer = new Color[width][height];
        this.zbuffer = new double[width][height];
        this.initBuffers();
        this.notifyFrameChange();
    }

    public void setPolygonMesh(PolygonMesh mesh) {
        this.polygonMesh = mesh;
        this.zoffset = 0; // no zoom
        this.notifyFrameChange();
    }

    public void setBackfaceCulling(boolean culling) {
        this.backfaceCulling = culling;
        this.notifyFrameChange();
    }

    public double getXRotation() { return this.xrot;}
    public double getYRotation() { return this.yrot;}
    public double getZRotation() { return this.zrot;}
    public double getZoom() { return this.zoffset;}
    public void setXRotation(double r) { this.xrot = r; this.notifyFrameChange();}
    public void setYRotation(double r) { this.yrot = r; this.notifyFrameChange();}
    public void setZRotation(double r) { this.zrot = r; this.notifyFrameChange();}
    public void setZoom(double c) { this.zoffset = c; this.notifyFrameChange();}


    private void notifyFrameChange() {
        if(this.observer != null)
            this.observer.notifyFrameChanged();
    }
    public void setRendererObserver(RendererObserver o) {
        this.observer = o;
        this.notifyFrameChange();
    }

    private void initBuffers() {        
        for(int r = 0 ; r < this.width ; r++) {
            Arrays.fill(this.zbuffer[r], Double.POSITIVE_INFINITY);
            if(this.graphics == null)
                Arrays.fill(this.framebuffer[r], this.bgColor);
        }
        if(this.graphics != null) {
            this.graphics.setColor(this.bgColor);
            this.graphics.fillRect(0, 0, this.graphics.getClipBounds().width, this.graphics.getClipBounds().height);
        }
    }

    private void putPixel(Color c, int x, int y, double z) {
        if(x >= 0 && x < this.width && y >= 0 && y < this.height) {
            // valid pixel on viewing plane
            if(this.zbuffer[x][y] > z) { // depth test
                // pixel can really be drawn
                if(this.graphics != null) {
                    this.graphics.setColor(c);
                    this.graphics.drawLine(x, y, x, y);
                }
                else
                    this.framebuffer[x][y] = c;
                this.zbuffer[x][y] = z;
            }
        }
    }

    /**
     * Draw already projected line.
     * @param v1
     * @param v2
     * @param color
     */
    private void drawLine(GVertex v1, GVertex v2, Color color) {
        double Z = 0.0;
        int x1 = (int)Math.round(v1.getX()), y1 = (int)Math.round(v1.getY()),
            x2 = (int)Math.round(v2.getX()), y2 = (int)Math.round(v2.getY());

        double dx, dy;
        int steps;
        if(x1 == x2 && y1 == y2) {
            steps = 1;
            dx = dy = 1; // egal
        }
        else if(x1 == x2) {
            steps = (int)Math.abs(y2 - y1) + 1;
            dx = 0;
            dy = Math.signum(y2 - y1);
        }
        else if(y1 == y2) {
            steps = (int)Math.abs(x2 - x1) + 1;
            dx = Math.signum(x2 - x1);
            dy = 0;
        }
        else {
            if(Math.abs(x1 - x2) >= Math.abs(y1 - y2)) {
                dx = Math.signum(x2 - x1);
                dy = 1.0 * (y2 - y1) / Math.abs(x2 - x1);
                steps = (int)Math.abs(x1 - x2) + 1;
            }
            else {                
                dy = Math.signum(y2 - y1);
                dx = 1.0 * (x2 - x1) / Math.abs(y2 - y1);
                steps = (int)Math.abs(y1 - y2) + 1;
            }
        }

        double x = x1, y = y1;
        for(int i = 0 ; i < steps ; i++) {
            this.putPixel(color, (int)Math.round(x), (int)Math.round(y), Z);
            x += dx;
            y += dy;
        }
    }

    /**
     *
     * @param polygon Already projected polygon.
     */
    private void drawPolygonEdges(GPolygon3 polygon) {
        this.drawLine(polygon.v1, polygon.v2, Color.RED);
        this.drawLine(polygon.v2, polygon.v3, Color.RED);
        this.drawLine(polygon.v3, polygon.v1, Color.RED);
    }

    private GTransformation placeLightTransformation() {
        GTransformation placeTrans = new GTransformation();
        // - scale to fit in unit square
        double scale = Math.max(this.polygonMesh.getXmax() - this.polygonMesh.getXmin(),
                                this.polygonMesh.getYmax() - this.polygonMesh.getYmin());
        placeTrans.pushMatrix(GTransformation.scaleMatrix(1/scale, 1/scale, 1/scale));
        // - rescale to fit in screen
        scale = Math.min(this.width, this.height) * 0.5;
        placeTrans.pushMatrix(GTransformation.scaleMatrix(scale, scale, 1));
        // - place within viewing area (camera is -z => rotate to face
        //   camera with visible faces and move to z+)
        placeTrans.pushMatrix(GTransformation.rotationXMatrix(Math.PI));

        return placeTrans;
    }

    private GTransformation placeGraphTransformation() {
        GTransformation placeTrans = new GTransformation();
        // - scale to fit in unit square
        double scale = Math.max(this.polygonMesh.getXmax() - this.polygonMesh.getXmin(),
                                this.polygonMesh.getYmax() - this.polygonMesh.getYmin());
        placeTrans.pushMatrix(GTransformation.scaleMatrix(1/scale, 1/scale, 1/scale));
        // - user rotations
        placeTrans.pushMatrix(GTransformation.rotationZMatrix(this.zrot)); // by graph's z-axis
        placeTrans.pushMatrix(GTransformation.rotationYMatrix(this.yrot)); // by coordinate system y-axis
        placeTrans.pushMatrix(GTransformation.rotationXMatrix(this.xrot)); // by coordinate system x-axis
        // - place within viewing area (camera is -z => rotate to face
        //   camera with visible faces)
        placeTrans.pushMatrix(GTransformation.rotationXMatrix(Math.PI));
        //placeTrans.pushMatrix(GTransformation.scaleMatrix(1, -1, 1));
        double minz = Math.max(this.polygonMesh.getZmax(), Math.sqrt(2)) / scale;
        placeTrans.pushMatrix(GTransformation.translationMatrix(0, 0, minz + this.zoffset));        
        // - rescale to fit in screen
        scale = Math.min(this.width, this.height) * 0.9;
        placeTrans.pushMatrix(GTransformation.scaleMatrix(scale, scale, 1));
        return placeTrans;
    }

    /**
     * Render directly to graphics context (if <param>graphics</param> not null),
     * otherwise render to framebuffer.
     * @param graphics
     */
    public void render(Graphics graphics) {
        GVertex cameraPosition = new GVertex(0, 0, this.zcamera);

        this.graphics = graphics;
        // create transformation matrices
        Matrix graphPlaceMatrix = this.placeGraphTransformation().getTransformation();
        GTransformation projectionTransformation = new GTransformation();
        projectionTransformation.pushMatrix(GTransformation.perspectiveProjectionMatrix(-this.zcamera));
        // window placement matrix (move projection plane into the viewing area)
        projectionTransformation.pushMatrix(GTransformation.translationMatrix(this.width / 2, this.height / 2, 0));
        Matrix projectionMatrix = projectionTransformation.getTransformation();

        this.initBuffers();
        long renderedPolygonsCount = 0;
        long timeBegin = System.currentTimeMillis();
        for(GPolygon3 polygon : this.polygonMesh.getPolygons()) {
            // apply "placing" transformations to polygon
            GPolygon3 inScenePolygon = polygon.transform(graphPlaceMatrix);

            // is in view frustrum?
            // TODO better
            if(inScenePolygon.v1.getZ() < 0 || inScenePolygon.v2.getZ() < 0 || inScenePolygon.v3.getZ() < 0)
                continue;
            
            // check if polygon is oriented with it's visible face to camera
            // (backface culling)
            if(backfaceCulling) {
                // TODO by polygon center of gravity
                GVertex polygonCenter = inScenePolygon.v1;
                GVector lookVector = new GVector(cameraPosition, polygonCenter);
                GVector polygonNormal = inScenePolygon.getNormal();
                if(lookVector.dotMultiply(polygonNormal) >= 0)
                    // skip this polygon
                    continue;
            }

            // project polygon vertices to viewing plane
            GPolygon3 projectedPolygon = inScenePolygon.transform(projectionMatrix);

            // draw polygon edges
            this.drawPolygonEdges(projectedPolygon);

            // light model & shading
            // TODO
            renderedPolygonsCount++;
        }

        GEdge[] axes = {
            new GEdge(new GVertex(0, 0, 0), new GVertex(this.polygonMesh.getXmax() * 1.5, 0, 0)),
            new GEdge(new GVertex(0, 0, 0), new GVertex(0, this.polygonMesh.getYmax() * 1.5, 0)),
            new GEdge(new GVertex(0, 0, 0), new GVertex(0, 0, this.polygonMesh.getZmax() * 1.5))
        };
        for(GEdge rawAxis : axes) {
            GEdge placedAxis = rawAxis.transform(graphPlaceMatrix);
            GEdge projectedAxis = placedAxis.transform(projectionMatrix);
            this.drawLine(projectedAxis.v1, projectedAxis.v2, Color.GREEN);
        }
        
        this.graphics = null; // this.graphics can be not null only during
                              // invocation of this method

        if(this.observer != null) {
            long timeEnd = System.currentTimeMillis();
            int duration = (int)(timeEnd - timeBegin);
            this.observer.notifyRendered(renderedPolygonsCount, duration);
        }
    }

    /**
     *
     * @return Contents of the framebuffer. Note that if the last invocation
     *         of render method was given a valid Graphics object, the scene
     *         has not been rendered to framebuffer.
     */
    public Color[][] getFramebuffer() {
        return this.framebuffer;
    }
}
