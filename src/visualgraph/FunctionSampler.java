/*
 * Project: 2D Graph Visualiser
 * Author: David Chaloupka
 * Crated: 2011-05-22
 * Last change: 2011-05-23
 */

package visualgraph;

/**
 * Object that samples given 2D function. If for an (x,y) the function is
 * undefined, the value stored is null.
 * 
 * @author kvafy
 */
public class FunctionSampler {
    private Function2D f;
    private double xmin, xmax, ymin, ymax;
    private int xsamples, ysamples;

    private Double[][] sampled;
    private double sampleZMin, sampleZMax;

    public FunctionSampler(
            Function2D f,
            double xmin, double xmax, double ymin, double ymax,
            int xsamples, int ysamples) {
        this.f = f;
        this.xmin = xmin; this.xmax = xmax;
        this.ymin = ymin; this.ymax = ymax;
        this.xsamples = xsamples; this.ysamples = ysamples;
        this.sample();
    }

    /** Fills the this.sampled array by z values (ie. f(x,y) values). */
    private void sample() {
        sampleZMin = Double.POSITIVE_INFINITY;
        sampleZMax = Double.NEGATIVE_INFINITY;
        this.sampled = new Double[xsamples][ysamples];
        for(int xk = 0 ; xk < xsamples ; xk++) {
            double x = xmin + (xmax - xmin) * (1.0 * xk / (xsamples - 1));
            for(int yk = 0 ; yk < ysamples ; yk++) {
                double y = ymin + (ymax - ymin) * (1.0 * yk / (ysamples - 1));
                this.sampled[xk][yk] = f.evaluate(x, y);
                if(this.sampled[xk][yk] != null) { // is function defined for (x,y)?
                    sampleZMin = Math.min(sampleZMin, this.sampled[xk][yk]);
                    sampleZMax = Math.max(sampleZMax, this.sampled[xk][yk]);
                }
            }
        }
    }

    public double getXMin() { return this.xmin;}
    public double getXMax() { return this.xmax;}
    public double getYMin() { return this.ymin;}
    public double getYMax() { return this.ymax;}
    public double getZMin() { return this.sampleZMin;}
    public double getZMax() { return this.sampleZMax;}

    /**
     * @return Number of samples on x-axis.
     */
    public int getXSamplesCount() {
        return this.xsamples;
    }
    /**
     * @return Number of samples on y-axis.
     */
    public int getYSamplesCount() {
        return this.ysamples;
    }

    /**
     *
     * @param xi   x-index of value
     * @param yi   y-index of value
     * @return     The value f(x,y) for x ~ xi and y ~ yi, if xi and yi are
     *             valid indices to this.sampled. Otherwise returns null.
     */
    public Double getZ(int xi, int yi) {
        if(xi < 0 || yi < 0 || xi >= this.xsamples || yi >= this.ysamples)
            return null;
        else
            return this.sampled[xi][yi];
    }

    /**
     * @param xi
     * @return  Return x-value for xi index (it's given by xmin, xmax a xsamples
     *          values).
     */
    public double getXValue(int xi) {
        return this.xmin + (1.0 * xi / (this.xsamples - 1)) * (this.xmax - this.xmin);
    }
    /**
     * @param yi
     * @return  Return y-value for yi index (it's given by ymin, ymax a ysamples
     *          values).
     */
    public double getYValue(int yi) {
        return this.ymin + (1.0 * yi / (this.ysamples - 1)) * (this.ymax - this.ymin);
    }
}
