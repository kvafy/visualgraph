/*
 * Project: 2D Graph Visualiser
 * Author: David Chaloupka
 * Crated: 2011-05-25
 * Last change: 2011-05-25
 */

package visualgraph.primitives;

import java.util.LinkedList;
import java.io.*;
import java.util.regex.*;

/**
 * A set of polygons. The class also knows extreme values (min/max) on axises.
 * @author kvafy
 */
public class PolygonMesh {
    private LinkedList<GPolygon3> polygons;
    private double xmin, xmax, ymin, ymax, zmin, zmax;

    public PolygonMesh() {
        this.xmin = this.ymin = this.zmin = Double.POSITIVE_INFINITY;
        this.xmax = this.ymax = this.zmax = Double.NEGATIVE_INFINITY;
        this.polygons = new LinkedList<GPolygon3>();
    }

    private void registerVertexExtremes(GVertex v) {
        this.xmin = Math.min(this.xmin, v.getX());
        this.ymin = Math.min(this.ymin, v.getY());
        this.zmin = Math.min(this.zmin, v.getZ());
        this.xmax = Math.max(this.xmax, v.getX());
        this.ymax = Math.max(this.ymax, v.getY());
        this.zmax = Math.max(this.zmax, v.getZ());

    }

    public void addPolygon(GPolygon3 polygon) {
        this.registerVertexExtremes(polygon.v1);
        this.registerVertexExtremes(polygon.v2);
        this.registerVertexExtremes(polygon.v3);
        this.polygons.add(polygon);
    }

    public LinkedList<GPolygon3> getPolygons() {
        return polygons;
    }

    public double getXmax() {return this.polygons.size() != 0 ? xmax : 1;}
    public double getXmin() {return this.polygons.size() != 0 ? xmin : 0;}
    public double getYmax() {return this.polygons.size() != 0 ? ymax : 1;}
    public double getYmin() {return this.polygons.size() != 0 ? ymin : 0;}
    public double getZmax() {return this.polygons.size() != 0 ? zmax : 1;}
    public double getZmin() {return this.polygons.size() != 0 ? zmin : 0;}

    /**
     * Loads a polygon mesh from file. Every line is a vertex:
     * "x <space(s)> y <space(s)> z". Every triplet of valid lines are vertices
     * of a polygon (triplets don't overlap). The order vertices is as if we
     * sere looking at the polygon's visible face and went counter-clokwise
     * along it's border.
     * @param fileName
     * @return
     */
    public static PolygonMesh fromFile(String fileName) {
        PolygonMesh mesh = new PolygonMesh();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(new File(fileName)));
            String reNumber = "([+-]?\\d+(?:\\.\\d+(?:[eE][+-]?\\d+)?)?)";
            Pattern pattern = Pattern.compile("^\\s*" + reNumber + "\\s+" + reNumber + "\\s+" + reNumber + "\\s*$");
            int vertexInPolygon = 0;
            GVertex[] vextices = new GVertex[3];
            for(String line = reader.readLine() ; line != null ; line = reader.readLine()) {
                Matcher matcher = pattern.matcher(line);
                if(matcher.matches()) {
                    double x = Double.valueOf(matcher.group(1)),
                           y = Double.valueOf(matcher.group(2)),
                           z = Double.valueOf(matcher.group(3));
                    vextices[vertexInPolygon++] = new GVertex(x, y, z);
                    if(vertexInPolygon == 3) {
                        GPolygon3 polygon = new GPolygon3(vextices[0], vextices[1], vextices[2]);
                        mesh.addPolygon(polygon);
                        vertexInPolygon = 0;
                    }
                }
            }
        }
        catch(IOException ioex) {
            System.err.println("Can't read configuration file: " + ioex);
        }
        finally {
            try {
                if(reader != null) reader.close();
            } catch(IOException ioex) {}
        }
        return mesh;
    }
}
