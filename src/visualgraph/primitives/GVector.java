/*
 * Project: 2D Graph Visualiser
 * Author: David Chaloupka
 * Crated: 2011-05-22
 * Last change: 2011-05-24
 */

package visualgraph.primitives;


/**
 *
 * @author kvafy
 */
public class GVector {
    private Matrix m;

    public GVector(double x, double y, double z) {
        double[][] mdata = {{x, y, z, 0.0}};
        this.m = new Matrix(mdata);
    }

    public GVector(GVertex from, GVertex to) {
        double x = to.getX() - from.getX(),
               y = to.getY() - from.getY(),
               z = to.getZ() - from.getZ();
        double[][] mdata = {{x, y, z, 0.0}};
        this.m = new Matrix(mdata);
    }

    public GVector transform(Matrix transMatrix) {
        Matrix transformed = this.m.mult(transMatrix);
        double x = transformed.get(0, 0),
               y = transformed.get(0, 1),
               z = transformed.get(0, 2);
        return new GVector(x, y, z);
    }

    public GVector vectorMultiply(GVector v2) {
        GVector v1 = this;
        double x = v1.getY() * v2.getZ() - v1.getZ() * v2.getY(),
               y = v1.getZ() * v2.getX() - v1.getX() * v2.getZ(),
               z = v1.getX() * v2.getY() - v1.getY() * v2.getX();
        return new GVector(x, y, z);
    }

    public double dotMultiply(GVector v2) {
        GVector v1 = this;
        return v1.getX() * v2.getX()
             + v1.getY() * v2.getY()
             + v1.getZ() * v2.getZ();
    }

    public double magnitude() {
        double squareSum = Math.pow(this.getX(), 2)
                         + Math.pow(this.getY(), 2)
                         + Math.pow(this.getZ(), 2);
        return Math.sqrt(squareSum);
    }

    public GVector normalized() {
        double mag = this.magnitude();
        return new GVector(this.getX() / mag, this.getY() / mag, this.getZ() / mag);
    }

    @Override
    public String toString() {
        return "(" + m.get(0, 0) + "," + m.get(0, 1) + "," + m.get(0, 2) + ")";
    }

    public double getX() { return this.m.get(0, 0);}
    public double getY() { return this.m.get(0, 1);}
    public double getZ() { return this.m.get(0, 2);}
}
