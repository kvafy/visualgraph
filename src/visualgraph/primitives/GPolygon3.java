/*
 * Project: 2D Graph Visualiser
 * Author: David Chaloupka
 * Crated: 2011-05-22
 * Last change: 2011-05-22
 */

package visualgraph.primitives;


/**
 *
 * @author kvafy
 */
public class GPolygon3 {
    public GVertex v1, v2, v3;
    private GVector normal = null;

    /**
     * Create a triangle (polygon with 3 vertices).
     * Order of vertices is counter-clockwise pass when looking at the polygon
     * from the visible side. Vector product of edge v1-to-v2 x v2-to-v3 gives
     * normal vector identifying the visible face of the polygon (vector "comes"
     * from visible face).
     * @param v1
     * @param v2
     * @param v3
     */
    public GPolygon3(GVertex v1, GVertex v2, GVertex v3) {
        this.v1 = v1; this.v2 = v2; this.v3 = v3;
    }

    public GVector getNormal() {
        if(this.normal != null)
            return this.normal;

        GVector edge1 = new GVector(this.v1, this.v2),
                edge2 = new GVector(this.v2, this.v3);
        this.normal = ( edge1.vectorMultiply(edge2) ).normalized();
        return this.normal;
    }

    public GPolygon3 transform(Matrix transMatrix) {
        GVertex pv1 = this.v1.transform(transMatrix),
                pv2 = this.v2.transform(transMatrix),
                pv3 = this.v3.transform(transMatrix);
        return new GPolygon3(pv1, pv2, pv3);
    }

    @Override
    public String toString() {
        return "(polygon: " + v1 + " ; " + v2 + " ; " + v3 + ")";
    }
}
