/*
 * Project: 2D Graph Visualiser
 * Author: David Chaloupka
 * Crated: 2011-05-22
 * Last change: 2011-05-22
 */

package visualgraph.primitives;

/**
 * Imutable object.
 * 
 * @author kvafy
 */
public class GVertex {
    private Matrix m;

    public GVertex(double x, double y, double z) {
        double[][] mdata = {{x, y, z, 1.0}};
        this.m = new Matrix(mdata);
    }

    public GVertex transform(Matrix transMatrix) {
        Matrix transformed = this.m.mult(transMatrix);
        double w = transformed.get(0, 3);
        double x = transformed.get(0, 0) / w,
               y = transformed.get(0, 1) / w,
               z = transformed.get(0, 2) / w;
        return new GVertex(x, y, z);
    }

    public double distanceQuadratic(GVertex v2) {
        GVertex v1 = this;
        double sum = Math.pow(v1.m.get(0, 0) - v2.m.get(0, 0), 2)
                   + Math.pow(v1.m.get(0, 1) - v2.m.get(0, 1), 2)
                   + Math.pow(v1.m.get(0, 2) - v2.m.get(0, 2), 2);
        return sum;
    }

    public double distance(GVertex v2) {
        return Math.sqrt(this.distanceQuadratic(v2));
    }

    @Override
    public String toString() {
        return "[" + m.get(0, 0) + "," + m.get(0, 1) + "," + m.get(0, 2) + "]";
    }

    public double getX() {
        return this.m.get(0, 0);
    }
    public double getY() {
        return this.m.get(0, 1);
    }
    public double getZ() {
        return this.m.get(0, 2);
    }
}
