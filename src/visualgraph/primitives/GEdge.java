/*
 * Project: 2D Graph Visualiser
 * Author: David Chaloupka
 * Crated: 2011-05-25
 * Last change: 2011-05-25
 */

package visualgraph.primitives;

/**
 *
 * @author kvafy
 */
public class GEdge {
    public GVertex v1, v2;

    public GEdge(GVertex v1, GVertex v2) {
        this.v1 = v1; this.v2 = v2;
    }

    public GEdge transform(Matrix transMatrix) {
        GVertex pv1 = this.v1.transform(transMatrix),
                pv2 = this.v2.transform(transMatrix);
        return new GEdge(pv1, pv2);
    }
}
