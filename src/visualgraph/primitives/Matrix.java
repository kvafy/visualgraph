/*
 * Project: 2D Graph Visualiser
 * Author: David Chaloupka
 * Crated: 2011-05-22
 * Last change: 2011-05-22
 */

package visualgraph.primitives;

import java.util.Arrays;

/**
 * Imutable object.
 * 
 * @author kvafy
 */
public class Matrix {
    private double[][] m;

    private static boolean validateMatrix(double[][] data) {
        if(data == null || data[0] == null)
            return false;
        int cols = data[0].length;
        for(int r = 0 ; r < data.length ; r++)
            if(data[r] == null || data[r].length != cols)
                return false;
        return true;
    }

    public Matrix(double[][] data) {
        if(!Matrix.validateMatrix(data))
            throw new IllegalArgumentException("Invalid matrix data.");
        this.m = data;
    }

    public Matrix(int rows, int cols) {
        this.m = new double[rows][cols];
        for(int r = 0 ; r < rows ; r++)
            Arrays.fill(m[r], 0.0);
    }


    public int getRows() {
        return this.m.length;
    }
    public int getCols() {
        return this.m[0].length;
    }
    public double get(int r, int c) {
        return this.m[r][c];
    }
    public void set(int r, int c, double v) {
        this.m[r][c] = v;
    }

    public Matrix mult(Matrix m2) {
        Matrix m1 = this;
        if(m1.getCols() != m2.getRows())
            throw new IllegalArgumentException("Invalid matrix pair for matrix multiplication.");
        int rows = m1.getRows(),
            cols = m2.getCols(),
            items = m1.getCols();
        Matrix ret = new Matrix(rows, cols);
        for(int r = 0 ; r < rows ; r++) {
            for(int c = 0 ; c < cols ; c++) {
                double sum = 0;
                for(int i = 0 ; i < items ; i++)
                    sum += m1.get(r, i) * m2.get(i, c);
                ret.set(r, c, sum);
            }
        }
        return ret;
    }
}
