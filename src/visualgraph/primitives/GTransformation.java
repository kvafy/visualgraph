/*
 * Project: 2D Graph Visualiser
 * Author: David Chaloupka
 * Crated: 2011-05-22
 * Last change: 2011-05-23
 */

package visualgraph.primitives;

import java.util.LinkedList;

/**
 *
 * @author kvafy
 */
public class GTransformation {
    private LinkedList<Matrix> matrices = new LinkedList<Matrix>();
    private Matrix currentTransformation = null;


    public GTransformation() {
    }

    public void pushMatrix(Matrix m) {
        this.matrices.addLast(m);
        this.recountTransformation();
    }
    public void popMatrix() {
        this.matrices.removeLast();
        this.recountTransformation();
    }
    public Matrix getTransformation() {
        return this.currentTransformation;
    }

    private void recountTransformation() {
        Matrix m = GTransformation.identityMatrix();
        // multiply from left
        for(int i = 0 ; i < this.matrices.size() ; i++)
            m = m.mult(this.matrices.get(i));
        this.currentTransformation = m;
    }



    public static Matrix identityMatrix() {
        double mdata[][] = {{1,  0,  0,  0},
                            {0,  1,  0,  0},
                            {0,  0,  1,  0},
                            {0,  0,  0,  1}};
        return new Matrix(mdata);
    }
    public static Matrix translationMatrix(double dx, double dy, double dz) {
        double mdata[][] = {{1,  0,  0,  0},
                            {0,  1,  0,  0},
                            {0,  0,  1,  0},
                            {dx, dy, dz, 1}};
        return new Matrix(mdata);
    }
    public static Matrix scaleMatrix(double sx, double sy, double sz) {
        double mdata[][] = {{sx, 0,  0,  0},
                            {0,  sy, 0,  0},
                            {0,  0,  sz, 0},
                            {0,  0,  0,  1}};
        return new Matrix(mdata);
    }
    public static Matrix rotationXMatrix(double alpha) {
        double sin = Math.sin(alpha),
               cos = Math.cos(alpha);
        double mdata[][] = {{1,  0,    0,   0},
                            {0,  cos,  sin, 0},
                            {0,  -sin, cos, 0},
                            {0,  0,    0,   1}};
        return new Matrix(mdata);
    }
    public static Matrix rotationYMatrix(double alpha) {
        double sin = Math.sin(alpha),
               cos = Math.cos(alpha);
        double mdata[][] = {{cos,  0,  sin, 0},
                            {0,    1,  0,   0},
                            {-sin, 0,  cos, 0},
                            {0,    0,  0,   1}};
        return new Matrix(mdata);
    }
    public static Matrix rotationZMatrix(double alpha) {
        double sin = Math.sin(alpha),
               cos = Math.cos(alpha);
        double mdata[][] = {{cos,  sin,   0,   0},
                            {-sin, cos,   0,   0},
                            {0,    0,     1,   0},
                            {0,    0,     0,   1}
        };
        return new Matrix(mdata);
    }
    public static Matrix perspectiveProjectionMatrix(double dz) {
        double mdata[][] = {{1, 0, 0, 0},
                            {0, 1, 0, 0},
                            {0, 0, 0, 1.0 / dz},
                            {0, 0, 0, 1}
        };
        return new Matrix(mdata);
    }
    public static Matrix paralelProjectionMatrix() {
        double mdata[][] = {{1, 0, 0, 0},
                            {0, 1, 0, 0},
                            {0, 0, 0, 0},
                            {0, 0, 0, 1}
        };
        return new Matrix(mdata);
    }

}
