/*
 * Project: 2D Graph Visualiser
 * Author: David Chaloupka
 * Crated: 2011-05-22
 * Last change: 2011-05-24
 *
 * Changelog:
 * * 2011-05-24 FIX polygonize method didn't create whole mesh for different
 *                  counts of samples on x- and y-axis
 */

package visualgraph.primitives;

import java.util.LinkedList;
import visualgraph.FunctionSampler;

/**
 *
 * @author kvafy
 */
public class SquareGridMesher {
    /**
     * Order of vertices is counter-clockwise pass and vector product of edge
     * v1-to-v2 x v2-to-v3 gives normal vector identifying the visible face
     * of the polygon (vector "comes" from visible face).
     * 
     * @return Polygon with given vertices, if it exists (indices are valid
     *         withing array access bounds and sampled function is defined at
     *         that point). Otherwise return null.
     */
    private static GPolygon3 getPolygon(FunctionSampler sampler,
                                int xi1, int yi1,
                                int xi2, int yi2,
                                int xi3, int yi3) {
        Double z1 = sampler.getZ(xi1, yi1),
               z2 = sampler.getZ(xi2, yi2),
               z3 = sampler.getZ(xi3, yi3);
        if(z1 == null || z2 == null || z3 == null)
            return null;
        // polygon exists for sure
        GVertex v1 = new GVertex(sampler.getXValue(xi1),
                                 sampler.getYValue(yi1),
                                 z1);
        GVertex v2 = new GVertex(sampler.getXValue(xi2),
                                 sampler.getYValue(yi2),
                                 z2);
        GVertex v3 = new GVertex(sampler.getXValue(xi3),
                                 sampler.getYValue(yi3),
                                 z3);
        return new GPolygon3(v1, v2, v3);
    }

    private static GPolygon3 getInnerUpPolygonLeft(FunctionSampler sampler, int xi, int yi) {
        return SquareGridMesher.getPolygon(sampler,
                xi, yi,
                xi, yi + 1,
                xi - 1, yi);
    }
    private static GPolygon3 getInnerUpPolygonRight(FunctionSampler sampler, int xi, int yi) {
        return SquareGridMesher.getPolygon(sampler,
                xi, yi,
                xi + 1, yi,
                xi, yi + 1);
    }
    private static GPolygon3 getOuterUpPolygonLeft(FunctionSampler sampler, int xi, int yi) {
        return SquareGridMesher.getPolygon(sampler,
                xi, yi,
                xi, yi + 1,
                xi - 1, yi + 1);
    }
    private static GPolygon3 getOuterUpPolygonRight(FunctionSampler sampler, int xi, int yi) {
        return SquareGridMesher.getPolygon(sampler,
                xi, yi,
                xi + 1, yi + 1,
                xi, yi + 1);
    }

    /**
     * Create polygon mesh for function f(x,y).
     * 
     * @param sampler
     * @return Polygon mesh for sampled function in given ranges (ranges are
     *         stored withing <param>sampler</param> object).
     */
    public static PolygonMesh polygonize(FunctionSampler sampler) {
        PolygonMesh mesh = new PolygonMesh();

        for(int yi = 0 ; yi < sampler.getYSamplesCount() ; yi++) {
            boolean innerVertex = (yi % 2) == 0;
                for(int xi = 0 ; xi < sampler.getXSamplesCount() ; xi++) {
                GPolygon3 p1, p2;
                if(innerVertex) {
                    p1 = SquareGridMesher.getInnerUpPolygonLeft(sampler, xi, yi);
                    p2 = SquareGridMesher.getInnerUpPolygonRight(sampler, xi, yi);
                }
                else {
                    p1 = SquareGridMesher.getOuterUpPolygonLeft(sampler, xi, yi);
                    p2 = SquareGridMesher.getOuterUpPolygonRight(sampler, xi, yi);
                }
                if(p1 != null)
                    mesh.addPolygon(p1);
                if(p2 != null)
                    mesh.addPolygon(p2);

                innerVertex = !innerVertex;
            }
        }
        return mesh;
    }
}
