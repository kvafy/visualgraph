/*
 * Project: 2D Graph Visualiser
 * Author: David Chaloupka
 * Crated: 2011-05-24
 * Last change: 2011-05-24
 */

package visualgraph.expression;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Stack;
import java.util.regex.*;

/**
 * Parses an expression to it's RNP form. Expression object can then be
 * evaluated for different values of it's variables.
 * @author kvafy
 */
public class Expression {
    private HashMap<String, Double> variables;
    private LinkedList<Token> expressionRPN;

    private Expression() {
        this.variables = new HashMap<String, Double>();
        this.expressionRPN = null;
    }


    /**
     * @throws IllegalArgumentException On any error (lexical, parsing, ...).
     */
    public static Expression parse(String exprStr) {
        Expression exprObj = new Expression();
        try {
            LinkedList<Token> tokens = Expression.tokenize(exprStr, exprObj);
            // convert to RPN
            Stack<TokenOperator> operatorStack = new Stack<TokenOperator>(); // auxiliary stack for infix -> RPN conversion
            LinkedList<Token> rpnTokens = new LinkedList<Token>(); // converted result
            while(!tokens.isEmpty()) {
                Token t = tokens.pollFirst();
                if(t instanceof TokenValue) // numbers are copied to the output
                    rpnTokens.add(t);
                else { // operator
                    TokenOperator inputOperator = (TokenOperator)t;
                    if(inputOperator.oper.equals("("))
                        operatorStack.push(inputOperator);
                    else if(inputOperator.oper.equals(")")) {
                        // ")" => pop from stack everything to top most "("
                        while(!operatorStack.empty() && !operatorStack.peek().oper.equals("("))
                            rpnTokens.add(operatorStack.pop());
                        operatorStack.pop(); // pop the "("
                    }
                    else {
                        // "normal" operator - not "(" nor ")"
                        while(!operatorStack.empty() && !operatorStack.peek().oper.equals("(")
                                && operatorStack.peek().getPriority() >= inputOperator.getPriority())
                            // first pop operators with higher or equal priority (left associative)
                            rpnTokens.add(operatorStack.pop());
                        operatorStack.push((TokenOperator)t);
                    }
                }
            }
            while(!operatorStack.empty())
                rpnTokens.add(operatorStack.pop());

            exprObj.expressionRPN = rpnTokens;

            // set known constants
            exprObj.setVariable("E", Math.E);
            exprObj.setVariable("PI", Math.PI);

            // try to evaluate (is expression valid?)
            exprObj.evaluate();
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Invalid expression " + exprStr);
        }

        return exprObj;
    }


    private static LinkedList<Token> tokenize(String exprStr, Expression exprObj) {
        LinkedList<Token> tokens = new LinkedList<Token>();
        // note: group(1) is always the first token in string,
        Pattern reOper = Pattern.compile("^([)(+*/^-]|(?:sin|cos)\\b).*"),
                reId   = Pattern.compile("^([a-zA-Z]+).*"),
                reNum  = Pattern.compile("^([0-9]+(?:\\.[0-9]+)?).*");
        exprStr = exprStr.trim();
        while(!exprStr.isEmpty()) {
            Matcher matcherOper = reOper.matcher(exprStr),
                    matcherId   = reId.matcher(exprStr),
                    matcherNum  = reNum.matcher(exprStr);
            if(matcherOper.matches()) {
                String oper = matcherOper.group(1);
                tokens.add(new TokenOperator(oper));
                exprStr = exprStr.substring(oper.length());
            }
            else if(matcherId.matches()) {
                String id = matcherId.group(1);
                tokens.add(new TokenVariable(exprObj, id));
                exprObj.variables.put(id, 0.0); // install the variable
                exprStr = exprStr.substring(id.length());
            }
            else if(matcherNum.matches()) {
                String numStr = matcherNum.group(1);
                Double num = Double.parseDouble(numStr);
                tokens.add(new TokenConstant(num));
                exprStr = exprStr.substring(numStr.length());
            }
            else
                throw new IllegalArgumentException("unable to tokenize: " + exprStr);

            exprStr = exprStr.trim();
        }
        return tokens;
    }

    /**
     * Evaluate the expression for current values of variables.
     * @return The value of expression.
     */
    public double evaluate() {
        Stack<Double> numberStack = new Stack<Double>();
        for(int i = 0 ; i < this.expressionRPN.size() ; i++) {
            Token t = this.expressionRPN.get(i);
            if(t instanceof TokenValue)
                numberStack.push(((TokenValue)t).getValue());
            else { // operator
                TokenOperator operator = (TokenOperator)t;
                int arity = operator.getArity();
                if(numberStack.size() < arity)
                    throw new IllegalArgumentException("Not enough operands for operator " + operator.oper);
                double[] operands = new double[arity];
                for(int j = 0 ; j < arity ; j++)
                    operands[arity - j - 1] = numberStack.pop();
                numberStack.push(operator.evaluate(operands));
            }
        }
        if(numberStack.size() != 1)
            throw new IllegalArgumentException("invalid expression");
        return numberStack.pop();
    }

    /**
     * @param var
     * @return Value of the variable, if it exists, or zero.
     */
    public double getVariable(String var) {
        if(this.variables.containsKey(var))
            return this.variables.get(var);
        else
            return 0.0;
    }
    public void setVariable(String var, double val) {
        if(this.variables.containsKey(var))
            this.variables.put(var, val);
    }
    /**
     *
     * @return List of variable names used in the expression.
     *         Note that the list can also contain constant names PI and E.
     */
    public String[] getVariableNames() {
        if(this.variables.isEmpty())
            return null;
        else {
            String[] ret = new String[this.variables.size()];
            this.variables.keySet().toArray(ret);
            return ret;
        }
    }
}







/** A token in expression:
 *    - number \d+(.\d+)?
 *    - identifier (alpha characters)
 *    - operator +-/*^
 */
class Token {
}


class TokenOperator extends Token {
    public final String oper;

    public static final HashSet<String> KNOWN_OPERATORS;
    public static final HashMap<String, Integer> ARITIES;
    public static final HashMap<String, Integer> PRIORITIES; // for evaluation (not syntax analysis!!)
    static {
        KNOWN_OPERATORS = new HashSet<String>();
        KNOWN_OPERATORS.add("+"); KNOWN_OPERATORS.add("-"); KNOWN_OPERATORS.add("*");
        KNOWN_OPERATORS.add("/"); KNOWN_OPERATORS.add("^"); KNOWN_OPERATORS.add("(");
        KNOWN_OPERATORS.add(")");
        KNOWN_OPERATORS.add("sin"); KNOWN_OPERATORS.add("cos");
        ARITIES = new HashMap<String, Integer>();
        ARITIES.put("+", 2); ARITIES.put("-", 2); ARITIES.put("*", 2); ARITIES.put("/", 2);
        ARITIES.put("^", 2);
        ARITIES.put("sin", 1); ARITIES.put("cos", 1);
        PRIORITIES = new HashMap<String, Integer>();
        PRIORITIES.put("+", 1); PRIORITIES.put("-", 1);
        PRIORITIES.put("*", 2); PRIORITIES.put("/", 2);
        PRIORITIES.put("^", 3); PRIORITIES.put("sin", 3); PRIORITIES.put("cos", 3);
    }

    public TokenOperator(String oper) {        
        if(!TokenOperator.KNOWN_OPERATORS.contains(oper))
            throw new IllegalArgumentException("unknown operator \"" + oper + "\"");
        this.oper = oper;
    }
    public int getPriority() {
        return TokenOperator.PRIORITIES.get(this.oper);
    }
    public int getArity() {
        return TokenOperator.ARITIES.get(this.oper);
    }
    /**
     *
     * @param args  Operands in order left to right (for "a+b" a = args[0], b = args[1])
     * @return
     */
    public double evaluate(double[] args) {
        if(this.getArity() != args.length)
            throw new IllegalArgumentException("invalid count of operands for operator " + this.oper);
        if(this.oper.equals("+"))      return args[0] + args[1];
        else if(this.oper.equals("-")) return args[0] - args[1];
        else if(this.oper.equals("*")) return args[0] * args[1];
        else if(this.oper.equals("/")) {
            if(args[1] == 0)
                //throw new IllegalArgumentException("operator \"/\" with zero");
                return Double.POSITIVE_INFINITY * args[0];
            return args[0] / args[1];
        }
        else if(this.oper.equals("^")) return Math.pow(args[0], args[1]);
        else if(this.oper.equals("sin")) return Math.sin(args[0]);
        else if(this.oper.equals("cos")) return Math.cos(args[0]);
        else
            throw new IllegalArgumentException("no rule to evaluate operator \"" + this.oper + "\"");
    }
}


/**
 * A value (constant or variable).
 * Value is not meant as in "expression" (result of an operation); value in
 * this context is terminal leaf in the imaginary abstract syntactic tree.
 */
abstract class TokenValue extends Token {
    public abstract double getValue();
}

class TokenConstant extends TokenValue {
    private double value;

    public TokenConstant(double value) {
        this.value = value;
    }

    public double getValue() {
        return this.value;
    }
}

class TokenVariable extends TokenValue {
    private String name;
    private Expression exprObject;

    public TokenVariable(Expression expr, String name) {
        this.exprObject = expr;
        this.name = name;
    }

    public double getValue() {
        return this.exprObject.getVariable(this.name);
    }
}
