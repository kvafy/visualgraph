/*
 * Project: 2D Graph Visualiser
 * Author: David Chaloupka
 * Crated: 2011-05-23
 * Last change: 2011-05-23
 */

package visualgraph;

/**
 *
 * @author kvafy
 */
public interface RendererObserver {
    public void notifyFrameChanged();
    public void notifyRendered(long polygonCount, int timeForFrameMS);
}
