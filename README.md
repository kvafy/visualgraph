# Overview
VisualGraph is a 3D graphics program interesting by not using any 3D libraries. Everyting is implemented from scratch - projection, transformations, visibility, back-face culling etc.

The program is capable of visualizing .stri meshes (check out bunny.stri). User can also enter custom mathematical functions of two variables and the function will be visualized in 3D.

Originated as part of preparations for the final exam of my Bachelor's degree studies.


# Usage
`$ ant jar`
`$ java -jar dist/VisualGraph.jar

